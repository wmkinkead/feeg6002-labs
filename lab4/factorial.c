#include <stdio.h>
#include <limits.h>
#include <math.h>

long maxlong(void){

    return LONG_MAX;
}

double upper_bound(long n){
    if (n < 6){
        return 719.;
    }
    else{
        return pow((double) n/2, (double) n);
    }
}

long factorial(long n){
    if (n<0){
        return -2;
    }

    else if(upper_bound(n)>maxlong()){
        return -1;
    }

    else{
        if (n>=1){
            return n * factorial(n-1);
        }
        else{
            return 1;
        }
    }
}



int main(void) {
    long i = 20;

    printf("factorial(%ld)=%ld\n",i,factorial(i));
    return 0;
}
